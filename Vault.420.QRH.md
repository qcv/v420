# QUICK REFERENCE HAND BOOK

# Vault 420 V1.2 - V1.3

[VISIT PRISONER](#visit-prisoner)

[LOCK PRISONER](#lock-prisoner)

[ESCAPE RULES](#escape-rules)


## VISIT PRISONER

Condition: All functions of the prison are normal

> **INFO**
>
> Before the visiting process, all guards outside should
> assess whether any suspicious buildings are outside the
> prison, including walls and the roof.

### CHECK STAGE

|           |                             |
| :-------- | --------------------------: |
| Lockdown  |                         END |
| Headcount | Minimum 3 Guards + 1 Warden |

### PREPARE STAGE

|                                 |       |
| :------------------------------ | ----: |
| 1 Warden In Control Room        | CHECK |
| 1 Guard In Portal X-Ray Station | CHECK |
| 1 Guard In Cell X-Ray Station   | CHECK |
| 1 Guard in auto panic railway   | CHECK |
| Obstruct Prisoner Bed           |   YES |
| Cell Door                       | CLOSE |
| Auto Panic                      |    ON |
| Bed Door                        | CLOSE |
| Portal Door                     | CLOSE |
| Lockdown                        |   END |
| Inner Portal                    | CLOSE |

Place visitor bed
(unless prisoner is being locked up)

### INVITE STAGE

|                                                |         |
| :--------------------------------------------- | ------: |
| Visitor Is Waiting Outside The Portal (Nether) | CONFIRM |
| Visitor Has No Items                           | CONFIRM |
| Inner Portal                                   |    OPEN |
| Visitor Uses The Nether Side Portal            | CONFIRM |
| Visitor Enters The Prison                      |   CHECK |

> **IF**
> Any Player Leaves After Inner Portal Is On /
> Visitor Is Wearing Armor Or Carrying Items:
> |          |       |
> | :------- | ----: |
> | Lockdown | START |

|              |       |
| :----------- | ----: |
| Inner Portal | CLOSE |
| Lockdown     | START |

Wait 5 Seconds

> **IF**
> More Than One Leaving Message After Inner Portal Is Off
> (Ignore Guards)
> |          |       |
> | :------- | ----: |
> | Lockdown | START |

**Optional:** Wait for server restart
(disable auto panic temporarily)

|              |       |
| :----------- | ----: |
| Time         | NIGHT |
| Lockdown     |   END |
| Guards Logon | CHECK |

### KILL CHECK STAGE

|                         |         |
| :---------------------- | ------: |
| Portal Door             |    OPEN |
| Visitor Logon           |   CHECK |
| Visitor Sleeps The Bed  | CONFIRM |
| Portal Door             |   CLOSE |
| Visitor Suffocated      |   CHECK |
| Visitor Respawns        |   CHECK |

> **INFO**
>
> All guards should be wary of any suspicious behaviors.

> **IF**
> Visitor Is Wearing Armor Or Carrying Items
> |          |       |
> | :------- | ----: |
> | Lockdown | START |

### CELL STAGE

|                                |          |
| :----------------------------- | -------: |
| Visitor Clicks Visitor Bed     |  CONFIRM |
| Visitor Enters The Cell Tunnel |    CHECK |
| Cell Door                      |     OPEN |
| Visitor Enters The Cell        |    CHECK |
| Cell Door                      |    CLOSE |

### EXIT STAGE

|                                     |       |
| :---------------------------------- | ----: |
| Prisoner Kills Visitor              | CHECK |
| Visitor Says Something In The Chat After Respawning  | CHECK |

Pick up visitor bed from hopper

> **INFO**
> 
> All guards should be wary of any suspicious behaviors. For
> example, change of username

|                                |       |
| :----------------------------- | ----: |
| All Guards In The Control Room | CHECK |
| Lockdown                       | START |

End


## LOCK PRISONER

**Use:**
*Visit Prisoner* process for Check, Prepare, Invite and Kill
Check stages

### LOCK STAGE

|                                |       |
| :----------------------------- | ----: |
| Obstruct prisoner bed          |    NO |
| Bed Door                       |  OPEN |
| Time                           | NIGHT |
| Prisoner Sleeps On The Bed     | CHECK |
| Prisoner Wakes Up              | CHECK |
| Obstruct prisoner bed          |   YES |
| Prisoner Suffocated            | CHECK |
| Bed Door                       | CLOSE |
| All Guards In The Control Room | CHECK |
| Lockdown                       | START |

End


## ESCAPE RULES

### COMMAND BLOCK SETUP

- Area ban indicator must be enabled
- Outer area bans must be enabled

One of the following conditions must be met:
- All Elder guardians are spawned
- Command Block mining fatigue is enabled

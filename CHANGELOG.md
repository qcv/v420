# Vault 420 V1

A recreation of Vault 69 in Bedrock Edition.

## Main differences from Vault 69:
- Added roof and pearl glitch detectors
- Removed inner ZPI
- Replaced outer ZPIs with regular area bans
- The prison is only 100 blocks tall
- Auto panic uses a player in a minecart instead of a cat detector
- Most tunnels are 2 blocks tall instead of 1
- Revamped most of the redstone
- To use the X-ray stations you need to push your self at the wall using the piston and then pearl into the wall


# Vault 420 V1.1

## Changes:
- The roof and pearl glitch detector can no longer be bypassed by eating chorus fruit
- The command blocks that summon elder guardians now work properly

# Vault 420 V1.2:

## Changes:
- The kill checks no longer cause you to respawn at y=1, which would automatically trigger suicide

# Vault 420 V1.3:

## Changes:
- Added outside decoration
- Fixed an issue with the roof detector
